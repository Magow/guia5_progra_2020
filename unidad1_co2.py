#  !/usr/bin/env python3
#   -*- coding:utf-8 -*-
from os import system
import operator

CO2_data = 'co2_emission.csv'

(COUNTRIE,
 CODE,
 YEAR,
 CO2T) = range(4)

#   Extracción de valores
def content():

    dic_CO2 = {}

    work_CO2 = open(CO2_data, 'r')

    for count, line in enumerate(work_CO2):
        if count != 0:
            temp_lines = line.split(",")
            countrie = temp_lines[COUNTRIE].strip()
            code = temp_lines[CODE].strip()
            year = temp_lines[YEAR].strip()
            CO2t = temp_lines[CO2T].strip()
            dic_CO2_temp = {"Code": code, year : CO2t}
            if dic_CO2.get(countrie):
                dic_CO2[countrie].update(dic_CO2_temp)
            else:
                dic_CO2[countrie] = dic_CO2_temp

    work_CO2.close()

    return dic_CO2

#   uso de diccionarios para almacenar información necesaria
def data_rec(dic_ready, dic_numbers, dic_prom):

    countries_data = [key for key in dic_ready
    if key != "Statistical differences" or key != "World"]
    countries_rep_number = []
    countries_prom = []
    final_value = 0
    final_count = 0
    return_total = 0

    for i in countries_data:
        for key, value in dic_ready.items():
            #   aparecen world y stat.... no son países así que son eliminados de los valores
            if key == "World" or key == "Statistical differences":
                continue
            elif i == key:
                temp_dic = dic_ready[i]
                temp_sum = 0
                temp_rep = 0
                temp_end = 0
                for key, value in temp_dic.items():
                    if key != "Code":
                        temp_rep += 1
                        temp_sum = temp_sum + float(value)
                        final_value += float(value)
                        final_count += 1

                temp_end = round((temp_sum/(temp_rep-1)), 2)
                countries_rep_number.append(temp_rep)
                countries_prom.append(temp_end)

    return_total = final_value/final_count
    dic_numbers = dict(zip(countries_data, countries_rep_number))
    dic_prom = dict(zip(countries_data, countries_prom))


    return dic_prom, dic_numbers, return_total


def analysis(dic_prom, dic_numbers, return_total):

    #   orden de los valores de cada país por casos
    sort_reports = sorted(dic_numbers.items(), key = operator.itemgetter(1), reverse = True)
    sort_prom = sorted(dic_prom.items(), key = operator.itemgetter(1), reverse = True)
    return_total = round(return_total, 2)

    #   simple Impresión a falta de una mejor solución en el momento
    print("El país con mayor número de registros ", end="")
    print("fue ",sort_reports[0][0], " con ", sort_reports[0][1], " registros")
    print("El país con menor número de registros ", end="")
    print("fue ",sort_reports[230][0], " con ", sort_reports[230][1], " registros")
    print("El país con mayor cantidad de toneladas de CO2 ", end="")
    print("fue ",sort_prom[0][0], " con ", sort_prom[0][1], " toneladas")
    print("El país con menor cantidad de toneladas de CO2 ", end="")
    #   problema que no entiendo en Kyrgysztan, primero solo toma los valores
    #   con codigo y luego nada (al pasarlo al diccionario si no me equivoco)
    print("fue ",sort_prom[229][0], " con ", sort_prom[229][1], " toneladas")
    print(f"El promedio de los datos entregados fue de: {return_total} toneladas\n")


def search(data):
    countrie_sought = input("Nombre: ")
    diccionario_interact = data[countrie_sought]
    print("Años  toneladas CO2")
    for key, value in diccionario_interact.items():
        if key != "Codigo":
            print(key, value)

def menu():

    print("Menú de acciones para base de datos 'países y CO2'")
    print('\n')
    print("(1) Elegir país para analizar")
    #print("(2) ")
    print("(2) Salir de la base de datos de países")

if __name__ == '__main__':

    #system('clear')
    dic_numbers = {}
    dic_prom = {}

    system('clear')

    dic_ready = content()
    dic_prom, dic_numbers, return_total = data_rec(dic_ready, dic_numbers, dic_prom)
    analysis(dic_prom, dic_numbers, return_total)

    while True:

        menu()
        print('\n')
        choice = input("Ingrese la acción a realizar: ")

        if choice == "1":
            print("Ingrese el nombre del país para extraer su información")
            search(data = dic_ready)

        #elif choice == "2":
        #    system('clear')
        #    print("Edite algún elemento de la base de datos")
        #    print('\n')
        #    prot_base = edit_elements(prot_base)

        elif choice == "2":
            print("Ha deseado salir del programa")
            break
        else:
            system('clear')
            print("El valor ingresado no entrega ninguna respuesta", end=" ")
            print(" ingrese una opción valida")
            print('\n')
